from past.builtins import execfile
import os

rootDir = os.listdir()
exDir = os.listdir('./exercism')

progIn = input('Choose your program -> {} ?'.format(rootDir))
folder = "./"


if "exercism".startswith(progIn):
  progIn = str(input('\nChoose your program -> {} ?'.format(exDir)))
  folder = "./exercism/"
  for prog in exDir:
    if prog.startswith(progIn):
      execfile('{}'.format(folder+prog))
else:
  for prog in rootDir:
    if prog.startswith(progIn):
      execfile('{}'.format(folder+prog))